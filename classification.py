#!/usr/bin/python

import pickle, os

from sklearn import svm
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer, TfidfVectorizer
from sklearn.metrics import precision_recall_fscore_support, accuracy_score
from sklearn.model_selection import KFold, cross_val_score, train_test_split
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.dummy import DummyClassifier
from stop_words import get_stop_words

import numpy as np

class ItemSelector(BaseEstimator, TransformerMixin):

    def __init__(self, key):
        self.key = key

    def fit(self, x, y=None):
        return self

    def transform(self, data_dict):
        return data_dict[self.key]
 
def get_scores(results):
    ''' return the nr of objective and subjective titles in the list 'results' ''' 
    obj = 0
    subj = 0
    for result in results:
        if result == 'O':
            obj += 1
        elif result == 'S':
            subj += 1
    print(" - Nr. objective headlines: " + str(obj))
    print(" - Nr. subjective headlines: " + str(subj))
    total = obj + subj
    obj_perc = float(obj) / float(total)
    subj_perc = float(subj) / float(total)
    print(" - Percentage objective headlines: {}".format(obj_perc))
    print(" - Percentage subjective headlines: {}".format(subj_perc))

def write_to_pickle(filename, object):
    ''' Store object inf pickle with filename (without .pickle) '''
    with open('pickle_data/' + filename+'.pickle','w') as f:
        pickle.dump(object, f)
        print("- {} has been stored in pickle_data/{}".format(filename, filename+".pickle"))

def load_pickle(filename):
    ''' load pickle with filename (without .pickle) from map with pickle files '''	
    with open('pickle_data/' + filename +'.pickle', 'rb') as f:
        return pickle.load(f)      

def train_test_first_scenario():
    ''' Train and test a separate classifier for each newspaper with kFold 
    cross-validation and store classifier in pickle  ''' 
    print("\n##### Scenario 1:")
    for paper_name in ['ad','nrc','trouw','volkskrant']:
        # load ngrams, features_dict and classes from pickles

        sents = load_pickle(paper_name + '_sents_annotated')
        feats = load_pickle(paper_name + '_feats_annotated')
        classes = load_pickle(paper_name + '_classes_annotated')

        dev_sents = sents[800:900]
        dev_feats = feats[800:900]
        dev_classes = classes[800:900]

        dev_data = {}
        dev_data['sents'] = dev_sents
        dev_data['feats'] = dev_feats

        train_sents = sents[:800]
        train_feats = feats[:800]
        train_classes = classes[:800]

        train_data = {}
        train_data['sents'] = train_sents
        train_data['feats'] = train_feats

        stop_words = list(get_stop_words('nl'))

        pipeline = Pipeline([
            ('features', FeatureUnion([
                ('char-n-grams',Pipeline([
                    ('selector', ItemSelector(key="sents")),
                    ('ngrams', TfidfVectorizer(
                                ngram_range = (1,5), 
                                analyzer='char', 
                                binary=True))
                                    ])),
                ('word-n-grams',Pipeline([
                    ('selector', ItemSelector(key="sents")),
                    ('ngrams', TfidfVectorizer(
                                ngram_range = (1,3), 
                                analyzer='word', 
                                binary=True, 
                                stop_words = stop_words))
                                    ])),                                    
                ('features',Pipeline([
                    ('selector', ItemSelector(key="feats")),
                    ('feats', DictVectorizer())
                                    ]))
                ])),
            ('classifier', svm.LinearSVC(class_weight='balanced'))])

        pipeline.fit(train_data, train_classes)
        predicted = pipeline.predict(dev_data)
        print("\nDev set scores {}:".format(paper_name))
        print("- Accuracy: {}".format(accuracy_score(dev_classes, predicted)))
        precision, recall, fscore, support = precision_recall_fscore_support(dev_classes, predicted, average='weighted')
        print("- Precision: {}\n- Recall: {}\n- F-score: {}".format(precision, recall, fscore))
        get_scores(predicted)
 
        train_sents = train_sents + dev_sents 
        train_feats = train_feats + dev_feats 
        train_classes = train_classes + dev_classes
        
        train_data = {}
        train_data['sents'] = train_sents
        train_data['feats'] = train_feats        
        
        test_sents = sents[900:]
        test_feats = feats[900:]
        test_classes = classes[900:]

        test_data = {}
        test_data['sents'] = test_sents
        test_data['feats'] = test_feats
        test_classes = np.asarray(test_classes)
        
        pipeline.fit(train_data, train_classes)
        predicted = pipeline.predict(test_data)
        print("\nTest set scores {}:".format(paper_name))
        print("- Accuracy: {}".format(accuracy_score(test_classes, predicted)))
        precision, recall, fscore, support = precision_recall_fscore_support(test_classes, predicted, average='weighted')
        print("- Precision: {}\n- Recall: {}\n- F-score: {}".format(precision, recall, fscore))
        get_scores(predicted)
        
        #write_to_pickle(paper_name + "_classifier_1", pipeline)
        
def train_test_second_scenario():
    ''' Train a single classifier on all newspaper sets, test separately on all 
    newspaper sets and store classifier in pickle '''
    print("\n##### Scenario 2: Loading train and test data for all papers")

    ad_sents = load_pickle('ad_sents_annotated')    
    ad_classes = np.asarray(load_pickle('ad_classes_annotated'))    
    ad_feats = load_pickle('ad_feats_annotated')
    ad_train_feats = ad_feats[:800]
    ad_dev_feats = ad_feats[800:900]
    ad_test_feats = ad_feats[900:]
    ad_train_sents = ad_sents[:800]
    ad_dev_sents = ad_sents[800:900]
    ad_test_sents = ad_sents[900:]
    ad_train_classes = ad_classes[:800]
    ad_dev_classes = ad_classes[800:900]
    ad_test_classes = ad_classes[900:]
    
    nrc_sents = load_pickle('nrc_sents_annotated')        
    nrc_classes = np.asarray(load_pickle('nrc_classes_annotated'))
    nrc_feats = load_pickle('nrc_feats_annotated')
    nrc_train_feats = nrc_feats[:800]
    nrc_dev_feats = nrc_feats[800:900]
    nrc_test_feats = nrc_feats[900:]
    nrc_train_sents = nrc_sents[:800]
    nrc_dev_sents = nrc_sents[800:900]
    nrc_test_sents = nrc_sents[900:]
    nrc_train_classes = nrc_classes[:800]
    nrc_dev_classes = nrc_classes[800:900]
    nrc_test_classes = nrc_classes[900:]

    trouw_sents = load_pickle('trouw_sents_annotated')    
    trouw_classes = np.asarray(load_pickle('trouw_classes_annotated'))
    trouw_feats = load_pickle('trouw_feats_annotated')
    trouw_train_feats = trouw_feats[:800]
    trouw_dev_feats = trouw_feats[800:900]
    trouw_test_feats = trouw_feats[900:]
    trouw_train_sents = trouw_sents[:800]
    trouw_dev_sents = trouw_sents[800:900]
    trouw_test_sents = trouw_sents[900:]
    trouw_train_classes = trouw_classes[:800]
    trouw_dev_classes = trouw_classes[800:900]
    trouw_test_classes = trouw_classes[900:]

    volkskrant_sents = load_pickle('volkskrant_sents_annotated')    
    volkskrant_classes = np.asarray(load_pickle('volkskrant_classes_annotated'))
    volkskrant_feats = load_pickle('volkskrant_feats_annotated')
    volkskrant_train_feats = volkskrant_feats[:800]
    volkskrant_dev_feats = volkskrant_feats[800:900]
    volkskrant_test_feats = volkskrant_feats[900:]
    volkskrant_train_sents = volkskrant_sents[:800]
    volkskrant_dev_sents = volkskrant_sents[800:900]
    volkskrant_test_sents = volkskrant_sents[900:]
    volkskrant_train_classes = volkskrant_classes[:800]
    volkskrant_dev_classes = volkskrant_classes[800:900]
    volkskrant_test_classes = volkskrant_classes[900:]

    stop_words = list(get_stop_words('nl'))

    pipeline = Pipeline([
            ('features', FeatureUnion([
                ('char-N-grams',Pipeline([
                    ('selector', ItemSelector(key="sents")),
                    ('Ngrams', TfidfVectorizer(
                                ngram_range = (1,5), 
                                analyzer='char', 
                                binary=True))
                                    ])),
                ('word-N-grams',Pipeline([
                    ('selector', ItemSelector(key="sents")),
                    ('Ngrams', TfidfVectorizer(
                                ngram_range = (1,3), 
                                analyzer='word', 
                                binary=True, 
                                stop_words = stop_words))
                                    ])),                                    
                ('features',Pipeline([
                    ('selector', ItemSelector(key="feats")),
                    ('feats', DictVectorizer())
                                    ]))
                ])),
            ('classifier', svm.LinearSVC(class_weight='balanced'))])

    train_sents = ad_train_sents + nrc_train_sents + trouw_train_sents + volkskrant_train_sents
    train_classes = train_classes = np.concatenate((ad_train_classes, nrc_train_classes))
    train_classes = np.concatenate((train_classes, trouw_train_classes))
    train_classes = np.concatenate((train_classes, volkskrant_train_classes))
    
    train_feats = np.concatenate((ad_train_feats, nrc_train_feats), axis=0)
    train_feats = np.concatenate((train_feats, trouw_train_feats), axis=0)
    train_feats = np.concatenate((train_feats, volkskrant_train_feats), axis=0)

    train_data = {}
    train_data['feats'] = train_feats
    train_data['sents'] = train_sents
    pipeline.fit(train_data, train_classes)

    ad_dev_data = {}
    ad_dev_data['feats'] = ad_dev_feats
    ad_dev_data['sents'] = ad_dev_sents
    ad_predicted = pipeline.predict(ad_dev_data)
    print("\nDev Scores AD:")
    print("- Accuracy: {}".format(accuracy_score(ad_dev_classes, ad_predicted)))
    precision, recall, fscore, support = precision_recall_fscore_support(ad_dev_classes, ad_predicted, average='weighted')
    print("- Precision: {}\n- Recall: {}\n- F-score: {}\n".format(precision, recall, fscore))
    get_scores(ad_predicted)

    nrc_dev_data = {}
    nrc_dev_data['feats'] = nrc_dev_feats
    nrc_dev_data['sents'] = nrc_dev_sents
    nrc_predicted = pipeline.predict(nrc_dev_data)   
    print("\nDev Scores NRC:")
    print("- Accuracy: {}".format(accuracy_score(nrc_dev_classes, nrc_predicted)))
    precision, recall, fscore, support = precision_recall_fscore_support(nrc_dev_classes, nrc_predicted, average='weighted')
    print("- Precision: {}\n- Recall: {}\n- F-score: {}\n".format(precision, recall, fscore))
    get_scores(nrc_predicted)
    
    trouw_dev_data = {}
    trouw_dev_data['feats'] = trouw_dev_feats
    trouw_dev_data['sents'] = trouw_dev_sents
    trouw_predicted = pipeline.predict(trouw_dev_data)
    print("\nDev Scores Trouw:")
    print("- Accuracy: {}".format(accuracy_score(trouw_dev_classes, trouw_predicted)))
    precision, recall, fscore, support = precision_recall_fscore_support(trouw_dev_classes, trouw_predicted, average='weighted')
    print("- Precision: {}\n- Recall: {}\n- F-score: {}\n".format(precision, recall, fscore))
    get_scores(trouw_predicted)

    volkskrant_dev_data = {}
    volkskrant_dev_data['feats'] = volkskrant_dev_feats
    volkskrant_dev_data['sents'] = volkskrant_dev_sents
    volkskrant_predicted = pipeline.predict(volkskrant_dev_data)
    print("\nDev Scores Volkskrant:")
    print("- Accuracy: {}".format(accuracy_score(volkskrant_dev_classes, volkskrant_predicted)))
    precision, recall, fscore, support = precision_recall_fscore_support(volkskrant_dev_classes, volkskrant_predicted, average='weighted')
    print("- Precision: {}\n- Recall: {}\n- F-score: {}\n".format(precision, recall, fscore))
    get_scores(volkskrant_predicted)

    train_feats = np.concatenate((ad_train_feats, ad_dev_feats), axis=0)
    train_feats = np.concatenate((train_feats, nrc_train_feats), axis=0)
    train_feats = np.concatenate((train_feats, nrc_dev_feats), axis=0)
    train_feats = np.concatenate((train_feats, trouw_train_feats), axis=0)
    train_feats = np.concatenate((train_feats, trouw_dev_feats), axis=0)
    train_feats = np.concatenate((train_feats, volkskrant_train_feats), axis=0)
    train_feats = np.concatenate((train_feats, volkskrant_dev_feats), axis=0)

    train_classes = np.concatenate((ad_train_classes, ad_dev_classes))
    train_classes = np.concatenate((train_classes, nrc_train_classes))
    train_classes = np.concatenate((train_classes, nrc_dev_classes))
    train_classes = np.concatenate((train_classes, trouw_train_classes))
    train_classes = np.concatenate((train_classes, trouw_dev_classes))
    train_classes = np.concatenate((train_classes, volkskrant_train_classes))
    train_classes = np.concatenate((train_classes, volkskrant_dev_classes))
    
    train_sents = ad_train_sents + ad_dev_sents + nrc_train_sents + nrc_dev_sents + trouw_train_sents + trouw_dev_sents + volkskrant_train_sents + volkskrant_dev_sents
    train_data = {}
    train_data['feats'] = train_feats
    train_data['sents'] = train_sents

    pipeline.fit(train_data, train_classes)
    
    ad_test_data = {}
    ad_test_data['feats'] = ad_test_feats
    ad_test_data['sents'] = ad_test_sents
    ad_predicted = pipeline.predict(ad_test_data)
    print("\nTest Scores AD:")
    print("- Accuracy: {}".format(accuracy_score(ad_test_classes, ad_predicted)))
    precision, recall, fscore, support = precision_recall_fscore_support(ad_test_classes, ad_predicted, average='weighted')
    print("- Precision: {}\n- Recall: {}\n- F-score: {}\n".format(precision, recall, fscore))
    get_scores(ad_predicted)

    nrc_test_data = {}
    nrc_test_data['feats'] = nrc_test_feats
    nrc_test_data['sents'] = nrc_test_sents
    nrc_predicted = pipeline.predict(nrc_test_data)        
    print("\nTest Scores NRC:")
    print("- Accuracy: {}".format(accuracy_score(nrc_test_classes, nrc_predicted)))
    precision, recall, fscore, support = precision_recall_fscore_support(nrc_test_classes, nrc_predicted, average='weighted')
    print("- Precision: {}\n- Recall: {}\n- F-score: {}\n".format(precision, recall, fscore))
    get_scores(nrc_predicted)

    trouw_test_data = {}
    trouw_test_data['feats'] = trouw_test_feats
    trouw_test_data['sents'] = trouw_test_sents
    trouw_predicted = pipeline.predict(trouw_test_data)
    print("\nTest Scores Trouw:")
    print("- Accuracy: {}".format(accuracy_score(trouw_test_classes, trouw_predicted)))
    precision, recall, fscore, support = precision_recall_fscore_support(trouw_test_classes, trouw_predicted, average='weighted')
    print("- Precision: {}\n- Recall: {}\n- F-score: {}\n".format(precision, recall, fscore))
    get_scores(trouw_predicted)

    volkskrant_test_data = {}
    volkskrant_test_data['feats'] = volkskrant_test_feats
    volkskrant_test_data['sents'] = volkskrant_test_sents
    volkskrant_predicted = pipeline.predict(volkskrant_test_data)
    print("\nTest Scores Volkskrant:")
    print("- Accuracy: {}".format(accuracy_score(volkskrant_test_classes, volkskrant_predicted)))
    precision, recall, fscore, support = precision_recall_fscore_support(volkskrant_test_classes, volkskrant_predicted, average='weighted')
    print("- Precision: {}\n- Recall: {}\n- F-score: {}\n".format(precision, recall, fscore))  
    get_scores(volkskrant_predicted)
    
    #write_to_pickle("classifier_2", pipeline)
    

def first_scenario():
    ''' using the classifiers from the first scenario, classify all remaining 
    titles of every newspaper with the classifiers of those papers'''
    for paper_name in ['ad','nrc','trouw','volkskrant']:
        print("\n#### Calculating scenario 1 for {}:".format(paper_name))
        clf = load_pickle(paper_name + "_classifier_1")
	
        feats = load_pickle(paper_name + "_feats")
        sents = load_pickle(paper_name + "_sents")
        data = {}
        data['feats'] = feats
        data['sents'] = sents
        
        predicted = clf.predict(data)
        get_scores(predicted) 
        
def second_scenario():                   
    ''' using the classifiers from the first scenario, classify all remaining 
    titles of every newspaper with the classifiers of those papers'''
    clf = load_pickle("classifier_2")
    for paper_name in ['ad','nrc','trouw','volkskrant']:
        
        print("\n#### Calculating scenario 2 for {}:".format(paper_name))
        feats = load_pickle(paper_name + "_feats")
        sents = load_pickle(paper_name + "_sents")
        data = {}
        data['feats'] = feats
        data['sents'] = sents
        
        predicted = clf.predict(data)
        get_scores(predicted)

def baseline1(treshold=0.20):
    ''' classify titles for all papers with lookups in the merged subjectivity 
    scores equal to or above the treshold are subjective and below the threshold
    are objective'''
    merged_lex = load_pickle("merged-lexicon")
    cwd = os.getcwd()
    
    for paper_name in ['ad','nrc','trouw','volkskrant']:
        print("\n##### Running baseline method 1 on " + paper_name)
        # TODO: implement on test and dev set
        sents = load_pickle(paper_name + "_sents_annotated")
        dev_sents = sents[800:900]
        test_sents = sents[900:]

        classes = load_pickle(paper_name + "_classes_annotated")
        dev_classes = classes[800:900]
        test_classes = classes[900:]

        dev_predicted = []
        for title in dev_sents:
            title = title.split()
            title_len = len(title)
            total_score = 0

            for word in title:
                # if word has subjectivity score -> add score to sum
                if word.rstrip() in merged_lex.keys(): 
                    total_score += merged_lex[word]

            total_score = total_score / title_len

            if total_score >= treshold:
                dev_predicted.append("S")
            else:
                dev_predicted.append("O")            

        print("\nDev scores {}".format(paper_name))
        print("- Accuracy: {}".format(accuracy_score(dev_classes, dev_predicted)))
        precision, recall, fscore, support = precision_recall_fscore_support(dev_classes, dev_predicted, average='weighted')
        print("- Precision: {}\n- Recall: {}\n- F-score: {}".format(precision, recall, fscore))  
        print("\nPredicted labels " + paper_name)
        get_scores(dev_predicted)
        print("\nAnnotated labels " + paper_name)
        get_scores(dev_classes) 

        test_predicted = []
        for title in test_sents:
            title = title.split()
            title_len = len(title)
            total_score = 0

            for word in title:
                # if word has subjectivity score -> add score to sum
                if word.rstrip() in merged_lex.keys(): 
                    total_score += merged_lex[word]

            total_score = total_score / title_len

            if total_score >= treshold:
                test_predicted.append("S")
            else:
                test_predicted.append("O")   

        print("\nTest scores {}".format(paper_name))
        print("- Accuracy: {}".format(accuracy_score(test_classes, test_predicted)))
        precision, recall, fscore, support = precision_recall_fscore_support(test_classes, test_predicted, average='weighted')
        print("- Precision: {}\n- Recall: {}\n- F-score: {}".format(precision, recall, fscore))  
        print("\nPredicted labels " + paper_name)
        get_scores(test_predicted)
        print("\nAnnotated labels " + paper_name)
        get_scores(test_classes)            		

def baseline2():
    for paper_name in ['ad','nrc','trouw','volkskrant']:
        print("\n##### Running baseline method 2 on " + paper_name)
        sents = load_pickle(paper_name + '_sents_annotated')
        classes = np.asarray(load_pickle(paper_name + '_classes_annotated'))
        
        train_sents = sents[:800]
        dev_sents = sents[800:900]
        test_sents = sents[900:]
        
        train_classes = classes[:800]
        dev_classes = classes[800:900]
        test_classes = classes[900:]
        
        clf = DummyClassifier(strategy='most_frequent')
        clf.fit(train_sents, train_classes)
        
        predicted = clf.predict(dev_sents)
        
        print("\nScores for dev set {}:".format(paper_name))
        print("- Accuracy: {}".format(accuracy_score(dev_classes, predicted)))
        precision, recall, fscore, support = precision_recall_fscore_support(dev_classes, predicted, average='weighted')
        print("- Precision: {}\n- Recall: {}\n- F-score: {}".format(precision, recall, fscore))  
        print("\nPredicted labels " + paper_name)
        get_scores(predicted)
        print("\nAnnotated labels " + paper_name)
        get_scores(dev_classes)    
        
        train_sents = np.concatenate((train_sents, dev_sents))
        train_classes = np.concatenate((train_classes, dev_classes))
        
        clf = DummyClassifier(strategy='most_frequent')
        clf.fit(train_sents, train_classes)
        
        predicted = clf.predict(test_sents)
        print("\nScores for test set {}:".format(paper_name))
        print("- Accuracy: {}".format(accuracy_score(test_classes, predicted)))
        precision, recall, fscore, support = precision_recall_fscore_support(test_classes, predicted, average='weighted')
        print("- Precision: {}\n- Recall: {}\n- F-score: {}".format(precision, recall, fscore))          
        print("\nPredicted labels " + paper_name)
        get_scores(predicted)
        print("\nAnnotated labels " + paper_name)
        get_scores(test_classes)          

def main():
    #train_test_first_scenario()
    #train_test_first_scenario_no_ngrams()
    #train_test_second_scenario()    
    baseline1()
    #baseline2()
    #first_scenario()
    #second_scenario()

if __name__ == "__main__":
	main()	
