# Bachelor thesis project #

### What is this repository for? ###

This repository contains the program used in my Bachelor's thesis for classifying Dutch newspapers' headlines as either objective or subjective. The research focuses on classifying titles from the papers: Het Algemeen Dagblad, Het NRC Handelsblad, De Trouw and De Volkskrant. The Lexicons directory contains a subjectivity lexicon created at Antwerp University named nl-sentiment.xml and a subjectivity lexicon created at UvA called all_assessments.txt. In the TwNC_scraper directory the code can be found which was used for scraping titles from the Twente Nieuws Corpus in scrapy.py. Also, the retrieved titles for all of the four papers can be found, which have been split in an annotated set of 1.000 and a set with remaining titles that have not been annotated. In the pickle_data directory, all the pickles that have been used can be found. Here are pickles containing the merged subjectivity lexicon and classes, custom features and sentences for each paper as well as classifiers for scenarios 1 and 2. In the readings directory some PDF-files of the papers that have been used as background literature can be found.

The program feats.py is responsible for merging the subjectivity lexicons from Antwerp University and the UvA and extracting the features from the titles. These are stored in pickles in the pickle_data directory, so they have to be extracted only once instead of for every approach.

The program classification.py is responsible for the different scenarios and baseline methods and trains and tests the classifiers for each approach on all papers. These classifiers are stored in the pickle_data directory as well. 
