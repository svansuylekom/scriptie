#!/usr/bin/python 

import spacy, sys, os, pickle
from xml.dom import minidom
import unicodedata
import nltk

from sklearn.feature_extraction.text import CountVectorizer

def read_sentiment():
    ''' Read nl-sentiment.xml and store in dict '''
    print("- Reading nl-sentiment.xml ...")
    sent_lex = {}
    cwd = os.getcwd()
    doc = minidom.parse(cwd + "/Lexicons/nl-sentiment.xml")
    for element in doc.getElementsByTagName("word"):
        # Extract word, POS-tag and subjectivity score and store in dict
        word = element.getAttribute("form").encode("ascii","ignore")
        subj_score = float(element.getAttribute("subjectivity"))
        sent_lex[word] = (subj_score) 

    return sent_lex

def read_assesments():
    ''' store all assessments in dict and return dict '''
    cwd = os.getcwd()
    print("- Reading all-assessments.txt ... ")
    assessments_lex = {}
    with open(cwd + "/Lexicons/all_assessments.txt","r") as f:
        for line in f:
            #first element and annotations are seperated by '\t'
            elements = line.split("\t")    
            try:
                word, POS = elements[0].split(" ") #word and POS-tag are seperated by ' '
                word = word.lower()
                if len(elements) == 2: #one annotator
                    sense = elements[1].rstrip()
                    subj_score = sense_to_float(sense)

                elif len(elements) == 3: #two annotators
                    sense1 = elements[1]
                    sense2 = elements[2].rstrip()
                    if sense1 == sense2: #both annotators agree
                        subj_score = sense_to_float(sense1)
                    else: #annotators disagree -> average of annotations
                        subj_score = avg_sense_to_float(sense1, sense2)
                
                if subj_score != -1:
                    assessments_lex[word] = (subj_score)
            except ValueError:
                # In case of bigrams (7 cases) -> excluded from dataset
                pass

    return assessments_lex

def merge_lexicons(x, y):
    ''' Merge both dictionaries and calculate average where necessary '''
    # !!! POS-tags are not matched yet -> takes value of y !!!
    print("- Merging lexicons ...")
    for key in x.keys():
        if key in y.keys():
            if x[key] == y[key]: #if subjectivity scores match -> store in dict
                pass
            else:
                if x[key] == None:
                    pass
                elif y[key] == None:
                    y[key] = x[key]
                else:
                    # if scores don't match -> store average in dict
                    y[key] = (x[key] + y[key]) / 2 
                    
        else:
            y[key] = x[key] #if entry does not exist in y yet -> copy
    return y

def avg_sense_to_float(sense1, sense2):
    ''' In case of annotator disagreement calculate average of annotations '''
    scores = {'--':-1.0, '-':-0.5, '0':0.0, '+':0.5, '++':1.0}
    if sense1 != '?' and sense2 != '?':
        return abs((scores[sense1] + scores[sense2]) / 2)
    elif sense1 == '?' and sense2 == '?':
        return -1 # Leave ? out of consideration
    elif sense1 == '?':
        return scores[sense2]
    elif sense2 == '?':
        return scores[sense1]

def sense_to_float(sense):
    ''' Convert ++, +, 0, -, -- into float as subjectivity score '''
    if sense == '0':
        return 0.0
    elif sense in ['-','+']:
        return 0.5
    elif sense in ['--','++']:
        return 1.0
    elif sense == '?':
        return -1


def word_ngrams(sentence, ngrams):
    ''' Add n-grams (1 to 3) in 'sentence' to the dict 'ngrams' '''
    for n in (1,2,3):
        for gram in nltk.ngrams(sentence, n):
            if len(gram) == 1:
                key = str(gram[0])
            else:
                key = ""
                for word in gram:
                    key += str(word) + " "

            if key in ngrams.keys():
                ngrams[key] += 1
            else:
                ngrams[key] = 1
    return ngrams

def extract_feats(sentence, nlp, merged_lex):
    ''' Parse given sentence and return dict with POS-tags count, title length, 
        quotes and subjectivity score'''
    reload(sys)
    sys.setdefaultencoding('utf8')  
    feats = {}
    # convert to unicode for spacy
    doc = nlp(unicode(sentence.rstrip(), errors='replace')) 
    nr_words = 0
    feats['quote'] = False
    subj_score = 0
    
    tags = ["ADJ","ADP","ADV","AUX","CONJ","CCONJ","DET","INTJ","NOUN",
            "NUM","PART","PRON","PROPN","PUNCT","SCONJ","SYM","VERB","X","SPACE"]
    # For each SpaCy POS-tag make a feature with nr. of occurences of that tag 
    for tag in tags:
        feats[tag] = 0

    for token in doc: 
        # read word and pos-tag and convert them to ascii format
        word = token.text.encode("ascii","ignore")
        pos = token.pos_.encode("ascii","ignore")
        
        if pos in tags:
            feats[pos] += 1
        else:
            print("{} is not a matching tag".format(pos))

        if pos == "PUNCT" and word in ['"',"'",':',';']:
            feats['quote'] = True # set quote to true if quotations in title
        nr_words += 1

        if word in merged_lex.keys(): 
            # if word has subjectivity score -> add score to sum
            subj_score += merged_lex[word]

	feats['subj_score'] = subj_score
	feats['normalized_subj_score'] = subj_score / nr_words
    feats['title_length'] = nr_words
    return feats

def read_paper(paper_name, nlp, merged_lex):
    ''' Returns list of parsed titles for the given paper'''
    cwd = os.getcwd()
    sents = []
    all_feats = []
    c = 1
   
   # Opens corresponding corpus and reads all titles
    with open(cwd + "/TwNC_scraper/" + paper_name + ".txt", "r") as f:
        for line in f:
            if len(line.split(" ")) > 2: #only titles with at least 3 words
                sents.append(line.rstrip())
                feats = extract_feats(line, nlp, merged_lex)
                all_feats.append(feats)
            if c%1000 == 0:
                print("- parsed {}".format(c))
            c += 1
    return all_feats, sents

def write_to_pickle(filename, object):
    ''' Store object inf pickle with filename (without .pickle) '''
    with open('pickle_data/' + filename+'.pickle','w') as f:
        pickle.dump(object, f)
        print("- {} has been stored in pickle_data/{}".format(filename, filename+".pickle"))    

def load_pickle(filename):
    ''' load pickle with filename (without .pickle) from map with pickle files '''
    with open('pickle_data/' + filename +'.pickle', 'r') as f:
        return pickle.load(f)       

def read_train_data(papername, nlp, merged_lex):
    ''' Get the training data and extract parsed sentences, the corresponding 
        classes and n_grams '''
    cwd = os.getcwd()
    sents = []
    with open(cwd + "/TwNC_scraper/" + papername +"_annotated.txt",'r') as f:
        all_feats = []
        obj_class = []
        for line in f:
            title, label = line.split('\t')
            sents.append(title.rstrip())
            feats = extract_feats(title.rstrip(), nlp, merged_lex)
            all_feats.append(feats)
            obj_class.append(label.rstrip())            
    return all_feats, obj_class, sents

def main():
    print("##### Importing lexicons ...")
    '''
    sent_lex = read_sentiment()
    assessments_lex = read_assesments()
    merged_lex = merge_lexicons(sent_lex,assessments_lex)
    write_to_pickle("merged-lexicon",merged_lex)
	'''
    merged_lex = load_pickle("merged-lexicon")

    nlp = spacy.load("nl_core_news_sm")
    '''
    print("##### Parsing annotated data ...")
    for paper_name in ["ad","nrc","trouw","volkskrant"]:
        print("- Parsing {}_annotated.txt ... ".format(paper_name))
        all_feats, obj_class, sents = read_train_data(paper_name ,nlp, merged_lex)
        write_to_pickle(paper_name + "_feats_annotated",all_feats)
        write_to_pickle(paper_name + "_classes_annotated",obj_class)
        write_to_pickle(paper_name + "_sents_annotated",sents)
    '''
    print("##### Parsing newspaper datasets ... ")
    for paper_name in ["ad","nrc","trouw","volkskrant"]:
        print("- Parsing {} ... ".format(paper_name)) 
        all_feats, sents = read_paper(paper_name, nlp, merged_lex)
        write_to_pickle(paper_name + "_feats", all_feats)
        write_to_pickle(paper_name + "_sents", sents)
    
if __name__ == "__main__":
    main()
