#!/usr/bin/python3
import os
import re
from xml.etree import ElementTree

def listToFile(title_list, filename):
    data = ""
    for title in title_list:
        if len(title.split(' ')) > 2:
            data += title + '\n'
        else:
            pass
    with open(filename, 'w') as f:
       f.write(data) 

def getPaper(filename):
    ''' Determine the name of the paper and return it '''
    if re.match('ad[.]*', filename) != None:
        return 'ad'
    elif re.match('volkskrant[.]*', filename) != None:
        return 'volkskrant'
    elif re.match('nrc[.]*', filename) != None:
        return 'nrc'
    elif re.match('trouw[.]*', filename) != None:
        return 'trouw'
    else:
        return None

def getTitles(xml_tree, paper_list):
    ''' Extract titles from xml_tree and add them to paper_list '''
    #titles = xml_tree.findall('ti')
    for node in xml_tree.findall("./artikel/body/ti/p"):
        try:
            paper_list.append(node.text.rstrip())
        except AttributeError:
            pass

def main():
    dirs = []
    files = []
    ad = []
    nrc = []
    volkskrant = []
    trouw = []
    path = "/net/corpora/TwNC-0.2/database/2005/"

    print("Reading Files ...")
    for directory in os.listdir(path):
        dirs.append(directory)

    print("Extracting data ...")
    for dir in dirs:
        directory = path + dir + '/' 
        filenames = [f for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))]
        for filename in filenames:
            with open(directory + filename, 'r') as fil:
                paper = getPaper(filename)
                if paper != None:
                    xml_tree = ElementTree.parse(fil)
                    if paper == 'ad':
                        getTitles(xml_tree, ad)
                    elif paper  == 'volkskrant':
                        getTitles(xml_tree, volkskrant)
                    elif paper == 'nrc':
                        getTitles(xml_tree, nrc)
                    elif paper == 'trouw':
                        getTitles(xml_tree, trouw)
                else:
                    pass

    print("Writing data to files ...")
    listToFile(ad, 'ad.txt')
    listToFile(volkskrant, 'volkskrant.txt')
    listToFile(nrc,'nrc.txt')
    listToFile(trouw,'trouw.txt')
    print("Done!")

if __name__ == "__main__":
    main()